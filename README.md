# OkHttpFinal

 **本项目是基于开源项目OkHttpFinal进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/pengjianbo/OkHttpFinal )追踪到原项目版本**

 #### 项目介绍

 - 项目名称：OkHttpFinal
 - 所属系列：ohos的第三方组件适配移植
 - 功能：一个对OkHttp封装的简单易用型HTTP请求和文件下载管理框架。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人：hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/pengjianbo/OkHttpFinal
 - 编程语言：Java
 - 外部库依赖：无
 - 原项目基线版本：v2.0.6 , sha1:0e5a00091f64034f31b49e27b4242cd7b6992c6e

 #### 演示效果

<img src="gif/demo.gif"/>

 #### 安装教程

##### 方法一：

  1. 编译依赖har包OkHttpFinal.har。
  2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
  3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

 ```
 dependencies {
     implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
 	……
 }
 ```

 4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方法二：

 1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

 ```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
 ```

 2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

 ```
 dependencies {
     implementation 'cn.finalteam.ohos:okhttpfinal:1.0.1'
 }
 ```

 #### 使用说明

 1. 请求接口(包括文件)
 ```java
 		List<File> files = new ArrayList<>();
        File file = new File("...");
        RequestParams params = new RequestParams(this);//请求参数
        params.addFormDataPart("username", mUserName);//表单数据
        params.addFormDataPart("password", mPassword);//表单数据
        params.addFormDataPart("file", file);//上传单个文件
        params.addFormDataPart("files", files);//上传多个文件
        params.addHeader("token", token);//添加header信息
        HttpRequest.post(Api.LOGIN, params, new BaseHttpRequestCallback<LoginResponse>() {
        
        //请求网络前
        @Override
        public void onStart() {
        	buildProgressDialog().show();
        }
        
        //这里只是网络请求成功了（也就是服务器返回JSON合法）没有没有分装具体的业务成功与失败，大家可以参考demo去分装自己公司业务请求成功与失败
        @Override
        protected void onSuccess(LoginResponse loginResponse) {
        	toast(loginResponse.getMsg());
        }
        
        //请求失败（服务返回非法JSON、服务器异常、网络异常）
        @Override
        public void onFailure(int errorCode, String msg) {
        	toast("网络异常~，请检查你的网络是否连接后再试");
        }
         
        //请求网络结束   
        @Override
        public void onFinish() {
            dismissProgressDialog();
        }
 ```
 2. 下载文件
 ```java
        tring url = "http://219.128.78.33/apk.r1.market.hiapk.com/data/upload/2015/05_20/14/com.speedsoftware.rootexplorer_140220.apk";
        File saveFile = new File("/sdcard/rootexplorer_140220.apk");
        HttpRequest.download(url, saveFile, new FileDownloadCallback() {
            //开始下载
            @Override 
            public void onStart() {
                super.onStart();
            }
        
        	//下载进度
            @Override
            public void onProgress(int progress, long networkSpeed) {
                super.onProgress(progress, networkSpeed);
                mPbDownload.setProgress(progress);
                //String speed = FileUtils.generateFileSize(networkSpeed);
            }
        
        	//下载失败
            @Override 
            public void onFailure() {
                super.onFailure();
                Toast.makeText(getBaseContext(), "下载失败", Toast.LENGTH_SHORT).show();
            }
        
        	//下载完成（下载成功）
            @Override 
            public void onDone() {
                super.onDone();
                Toast.makeText(getBaseContext(), "下载成功", Toast.LENGTH_SHORT).show();
            }
        });
 ```

 #### 版本迭代

 - v1.0.1

 #### 版权和许可信息
 - Apache Licence



