package cn.finalteam.okhttpfinal;

import java.util.concurrent.ConcurrentHashMap;

import okhttp3.Call;

/**
 * Desction:
 * Author:pengjianbo
 * Date:16/1/3 下午1:27
 */
class OkHttpCallManager {

    private ConcurrentHashMap<String, Call> callMap;
    private static OkHttpCallManager manager;

    private OkHttpCallManager() {
        callMap = new ConcurrentHashMap<>();
    }

    public static OkHttpCallManager getInstance() {
        if (manager == null) {
            manager = new OkHttpCallManager();
        }
        return manager;
    }

    public void addCall(String url, Call call) {
        if (call != null && !url.isEmpty()) {
            callMap.put(url, call);
        }
    }

    public Call getCall(String url) {
        if ( !url.isEmpty()) {
            return callMap.get(url);
        }

        return null;
    }

    public void removeCall(String url) {
        if ( !url.isEmpty() ) {
            callMap.remove(url);
        }
    }

}
