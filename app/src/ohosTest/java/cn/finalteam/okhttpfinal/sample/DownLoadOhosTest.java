/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.finalteam.okhttpfinal.sample;

import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static cn.finalteam.okhttpfinal.sample.EventHelper.triggerClickEvent;

/**
 * 需要保持网络畅通
 */
public class DownLoadOhosTest {
    DownloadAbility ability ;
    @Before
    public void startUp() throws Exception {
        ability = EventHelper.startAbility(DownloadAbility.class);
        Thread.sleep(2000);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() {
        Component component = ability.findComponentById(ResourceTable.Id_bt_download);
        ProgressBar mPbDownload = (ProgressBar) ability.findComponentById(ResourceTable.Id_pb_download);

        triggerClickEvent(ability, component);

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(100, mPbDownload.getProgress());
    }


}