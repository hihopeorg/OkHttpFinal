/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.finalteam.okhttpfinal.sample;

import cn.finalteam.okhttpfinal.sample.http.model.GameInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import ohos.agp.components.Text;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NewGameListOhosTest {
    NewGameListAbility ability ;
    @Before
    public void startUp() throws Exception {
        ability = EventHelper.startAbility(NewGameListAbility.class);
        Thread.sleep(2000);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName(){
        Text result = (Text) ability.findComponentById(ResourceTable.Id_tv_result);
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String stringValue=result.getText();
        String [] strings=stringValue.split(",");
        String value=strings[1];
        value.trim();
        String[] values=value.split("=");
        int textValue=Integer.valueOf(values[1]);
        Assert.assertEquals(textValue, 29);
    }

}