/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.finalteam.okhttpfinal.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Button;
import ohos.agp.components.ProgressBar;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 注意 该测试类 手动选择几个项， 否则会报错.
 * 1.需要手动 赋予读取权限的 系统授权弹框
 * 2.手动选择图片右上角 并点击 右下角使用
 */
public class UploadOhosTest {
    MainAbility mMainAbility;
    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();

    @After
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testBundleName() throws Exception {
        mMainAbility = EventHelper.startAbility(MainAbility.class);
        simulateClickOnScreen(546, 792);
        Thread.sleep(5000);
        IAbilityDelegator iAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        UploadAbility ability = (UploadAbility) iAbilityDelegator.getCurrentTopAbility();
        Thread.sleep(1000);
        ProgressBar mPbUpload = (ProgressBar) ability.findComponentById(ResourceTable.Id_pb_upload);
        Button button = (Button) ability.findComponentById(ResourceTable.Id_bt_upload);

        EventHelper.triggerClickEvent(ability,button);
        Thread.sleep(150000);

        Assert.assertEquals(100, mPbUpload.getProgress());

    }
    private void simulateClickOnScreen(int x, int y) {
        long downTime = Time.getRealActiveTime();
        long upTime = downTime+1;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, x, y);
        sAbilityDelegator.triggerTouchEvent(mMainAbility, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, x, y);
        sAbilityDelegator.triggerTouchEvent(mMainAbility, ev2);
    }

}