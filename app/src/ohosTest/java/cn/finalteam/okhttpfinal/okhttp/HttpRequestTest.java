/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.finalteam.okhttpfinal.okhttp;

import cn.finalteam.okhttpfinal.FileDownloadCallback;
import cn.finalteam.okhttpfinal.HttpRequest;
import cn.finalteam.okhttpfinal.JsonHttpRequestCallback;
import cn.finalteam.okhttpfinal.RequestParams;
import com.alibaba.fastjson.JSONObject;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class HttpRequestTest {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "HttpRequestTest");
    Context context;

    @Before
    public void setUp() throws Exception {
        HiLog.info(label, "测试开始");
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        context = sAbilityDelegator.getAppContext();
    }

    @After
    public void tearDown() throws Exception {
        HiLog.info(label, "测试完成");
    }


    @Test
    public void get() {
        Object mLock = new Object(); // 同步锁
        final boolean[] isFinish = new boolean[1];
        final int[] errorCode = new int[1];
        RequestParams params = new RequestParams();
        HttpRequest.get("https://wanandroid.com/wxarticle/chapters/json", params, new JsonHttpRequestCallback() {
            @Override
            protected void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                errorCode[0] = jsonObject.getIntValue("errorCode");
                isFinish[0] = true;
                System.out.println("get is onSuccess");
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
            }

            @Override
            public void onFailure(int errorCode, String msg) {
                super.onFailure(errorCode, msg);
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                System.out.println("get is onFailure");
            }
        });

        synchronized (mLock) {
            isFinish[0] = false;  // 设置锁条件
            while (isFinish[0] != true) {
                try {
                    mLock.wait(); // 等待唤醒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        assertEquals(0, errorCode[0]);
        System.out.println("get is finish");
    }


    @Test
    public void cancel() {
        Object mLock = new Object(); // 同步锁
        final boolean[] isFinish = new boolean[1];
        final int[] mErrorCode = new int[1];
        RequestParams params = new RequestParams();
        HttpRequest.put("https://www.wanandroid.com/banner/json", params, new JsonHttpRequestCallback() {
            @Override
            protected void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                System.out.println("cancel is onSuccess");
            }

            @Override
            public void onFailure(int errorCode, String msg) {
                super.onFailure(errorCode, msg);
                mErrorCode[0] = errorCode;
                isFinish[0] = true;
                System.out.println("cancel is onFailure----" + errorCode);
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
            }
        });
        HttpRequest.cancel("https://www.wanandroid.com/banner/json");

        synchronized (mLock) {
            isFinish[0] = false;  // 设置锁条件
            while (isFinish[0] != true) {
                try {
                    mLock.wait(); // 等待唤醒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        assertEquals(1003, mErrorCode[0]);
        System.out.println("post is finish");
    }

    @Test
    public void post() {
        Object mLock = new Object(); // 同步锁
        final boolean[] isFinish = new boolean[1];
        final int[] errorCode = new int[1];
        RequestParams params = new RequestParams();
        HttpRequest.post("https://www.wanandroid.com/banner/json", params, new JsonHttpRequestCallback() {
            @Override
            protected void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                errorCode[0] = jsonObject.getIntValue("errorCode");
                isFinish[0] = true;
                System.out.println("post is onSuccess");
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
            }

            @Override
            public void onFailure(int errorCode, String msg) {
                super.onFailure(errorCode, msg);
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                System.out.println("post is onFailure");
            }
        });

        synchronized (mLock) {
            isFinish[0] = false;  // 设置锁条件
            while (isFinish[0] != true) {
                try {
                    mLock.wait(); // 等待唤醒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        assertEquals(0, errorCode[0]);
        System.out.println("post is finish");
    }

    @Test
    public void delete() {
        Object mLock = new Object(); // 同步锁
        final boolean[] isFinish = new boolean[1];
        final int[] mErrorCode = new int[1];
        RequestParams params = new RequestParams();
        params.addFormDataPart("id", "4556142362143");
        HttpRequest.delete("https://www.wanandroid.com/lg/collect/deletetool/json", params, new JsonHttpRequestCallback() {
            @Override
            protected void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                System.out.println("delete is onSuccess");
            }

            @Override
            public void onFailure(int errorCode, String msg) {
                super.onFailure(errorCode, msg);
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                mErrorCode[0]=errorCode;
                System.out.println("delete is onFailure" + errorCode + msg);

            }
        });
        synchronized (mLock) {
            isFinish[0] = false;  // 设置锁条件
            while (isFinish[0] != true) {
                try {
                    mLock.wait(); // 等待唤醒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        assertEquals(405, mErrorCode[0]);
        System.out.println("delete is finish");
    }

    @Test
    public void download() {
        Object mLock = new Object(); // 同步锁
        final boolean[] isFinish = new boolean[1];
        final int[] mProgress = new int[1];
        String url = "https://imtt.dd.qq.com/16891/apk/049319033C352F315594F5DD4ACC2911.apk";
        String path = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        String filePath = path + File.separator + "moji.apk";
        HttpRequest.download(url, new File(filePath), new FileDownloadCallback() {
            @Override
            public void onStart() {
                super.onStart();
                isFinish[0] = false;
                System.out.println("test  download start");
            }

            @Override
            public void onProgress(int progress, long networkSpeed) {
                super.onProgress(progress, networkSpeed);
                mProgress[0] = progress;
            }

            @Override
            public void onFailure() {
                super.onFailure();
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                System.out.println("download fail---");
            }

            @Override
            public void onDone() {
                super.onDone();
                synchronized (mLock) {
                    mLock.notifyAll(); // 回调执行完毕，唤醒主线程
                }
                isFinish[0] = true;
                System.out.println("download done---" + mProgress[0]);
            }
        });
        synchronized (mLock) {
            isFinish[0] = false;  // 设置锁条件
            while (isFinish[0] != true) {
                try {
                    mLock.wait(); // 等待唤醒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        assertEquals(100, mProgress[0]);
        System.out.println("download is finish");
    }

}