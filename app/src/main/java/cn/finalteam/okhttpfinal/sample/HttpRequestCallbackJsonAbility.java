package cn.finalteam.okhttpfinal.sample;

import cn.finalteam.okhttpfinal.HttpRequest;
import cn.finalteam.okhttpfinal.JsonHttpRequestCallback;
import cn.finalteam.okhttpfinal.RequestParams;
import cn.finalteam.okhttpfinal.Utils;
import cn.finalteam.okhttpfinal.sample.http.Api;
import com.alibaba.fastjson.JSONObject;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;

public class HttpRequestCallbackJsonAbility extends BaseAbility {

    public Text result;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_http_request_callback_json);
        result = (Text) findComponentById(ResourceTable.Id_tv_result);
        RequestParams params = new RequestParams(this);
//        params.addFormDataPart("page", 1);
//        params.addFormDataPart("limit", 12);
        HttpRequest.post("https://www.wanandroid.com/banner/json", params, new JsonHttpRequestCallback() {
            @Override
            protected void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                result.setText(Utils.formatJson(jsonObject.toJSONString()));
            }
        });
    }
}
