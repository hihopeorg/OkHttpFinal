package cn.finalteam.okhttpfinal.sample.model;

public class UploadInfo {

    /**
     * sgin : null
     * url : https://mo.baidu.com/boxandroid?from=1023544j&source=1023534z
     */

    private Object sgin;
    private String url;

    public Object getSgin() {
        return sgin;
    }

    public void setSgin(Object sgin) {
        this.sgin = sgin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
