package cn.finalteam.okhttpfinal.sample.adapter;

import cn.finalteam.okhttpfinal.sample.http.MyHttpCycleContext;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.RecycleItemProvider;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.HashMap;
import java.util.List;

public abstract class BaseProvider<T> extends RecycleItemProvider {
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "BaseProvider");
    private List<T> mData;
    private MyHttpCycleContext mContext;

    public BaseProvider(List<T> data, MyHttpCycleContext context) {
        mData = data;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        if (component == null) {
            component = LayoutScatter.getInstance(mContext.getContext()).parse(getLayoutResId(), null, false);
            viewHolder = new ViewHolder();
            //将控件与 ViewHolder 绑定
            int[] viewIdArray = bindView();
            for (int viewId : viewIdArray) {
                viewHolder.bindViewById(component, viewId);
            }
            component.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) component.getTag();
        }
        initData(viewHolder, mData.get(position), position);
        return component;
    }


    public MyHttpCycleContext getContext() {
        return mContext;
    }

    /**
     * 获取 layout 文件
     */
    protected abstract int getLayoutResId();

    /**
     * 将控件与 ViewHolder 绑定
     */
    protected abstract int[] bindView();

    /**
     * 绑定显示数据,增加回调监听等操作
     */
    protected abstract void initData(ViewHolder holder, T t, int position);

    public class ViewHolder {
        private HashMap<Integer, Component> viewSparseArray;

        ViewHolder() {
            viewSparseArray = new HashMap<>();
        }

        /**
         * 通过 id获取 View
         */
        public <E extends Component> E getViewById(int id) {
            return (E) viewSparseArray.get(id);
        }

        /**
         * 通过 id 获取 View并绑定到 ViewHolder
         */
        public void bindViewById(Component view, int id) {
            viewSparseArray.put(id, view.findComponentById(id));
        }
    }





}
