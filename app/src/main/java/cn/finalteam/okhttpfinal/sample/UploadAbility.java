package cn.finalteam.okhttpfinal.sample;

import cn.finalteam.okhttpfinal.BaseHttpRequestCallback;
import cn.finalteam.okhttpfinal.HttpRequest;
import cn.finalteam.okhttpfinal.RequestParams;
import cn.finalteam.okhttpfinal.sample.http.model.UploadResponse;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.AbilityInfo;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;
import java.util.ArrayList;

public class UploadAbility extends BaseAbility {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "UploadAbility");

    private ProgressBar mPbUpload;
    private static final String EXTRA_RESULT_SELECTION = "extra_result_selection_path";
    private static final int RESULT_OK = 204;
    private String imgPath;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_upload);
        initView();
    }

    public void initView() {
        Button download = (Button) findComponentById(ResourceTable.Id_bt_upload);
        download.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                chooseImage();
            }
        });
        mPbUpload = (ProgressBar) findComponentById(ResourceTable.Id_pb_upload);
        Matisse.from(this)
                .choose(MimeType.ofAll())
                .countable(true)
                .maxSelectable(1)
                .thumbnailScale(0.85f)
                .showPreview(false);
    }

    private void chooseImage() {
        Matisse.from(this)
                .choose(MimeType.ofImage())
                .capture(true)
                .countable(true)
                .maxSelectable(30)
                .restrictOrientation(AbilityInfo.DisplayOrientation.PORTRAIT)
                .thumbnailScale(0.85f)
                .showSingleMediaType(true)
                .originalEnable(true)
                .forResult(1);
    }


    private void uploadFile(File file) {
        String fileuploadUri = "https://graph.baidu.com/upload";
        RequestParams params = new RequestParams(this);
//        params.addFormDataPart("image", file);
        HttpRequest.post(fileuploadUri, params, new BaseHttpRequestCallback<UploadResponse>() {
            @Override
            public void onSuccess(UploadResponse uploadResponse) {
                super.onSuccess(uploadResponse);
                HiLog.info(label, "上传成功--" + uploadResponse.getData());
                new ToastDialog(UploadAbility.this).setText("上传成功").show();
                mPbUpload.setProgressValue(100);
            }

            @Override
            public void onFailure(int errorCode, String msg) {
                super.onFailure(errorCode, msg);
                HiLog.info(label, "上传失败--" + msg + "---errorCode--" + errorCode);
                new ToastDialog(UploadAbility.this).setText("上传失败").show();
            }

            @Override
            public void onProgress(int progress, long networkSpeed, boolean done) {
                HiLog.info(label, "progress--" + progress);
                mPbUpload.setProgressValue(progress);
            }
        });
    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent data) {
        super.onAbilityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> arrayList = data.getStringArrayListParam(EXTRA_RESULT_SELECTION);
                imgPath = arrayList.get(0);
                uploadFile(new File(imgPath));
                HiLog.info(label, imgPath);
            }
        }
    }

}
