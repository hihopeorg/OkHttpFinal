package cn.finalteam.okhttpfinal.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.bundle.IBundleManager;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;

public class MainAbility extends Ability implements Component.ClickedListener {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        Button apiBean = (Button) findComponentById(ResourceTable.Id_btn_api_bean);
        Button apiString = (Button) findComponentById(ResourceTable.Id_btn_api_string);
        Button apiJsonObject = (Button) findComponentById(ResourceTable.Id_btn_api_jsonobject);
        Button dm = (Button) findComponentById(ResourceTable.Id_btn_dm);
        Button upload = (Button) findComponentById(ResourceTable.Id_btn_upload);
        Button download = (Button) findComponentById(ResourceTable.Id_btn_download);
        Button other = (Button) findComponentById(ResourceTable.Id_btn_other_funcation);
        apiBean.setClickedListener(this);
        apiString.setClickedListener(this);
        apiJsonObject.setClickedListener(this);
        dm.setClickedListener(this);
        upload.setClickedListener(this);
        download.setClickedListener(this);
        other.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_api_bean:
                goAbility(NewGameListAbility.class);
                break;
            case ResourceTable.Id_btn_api_string:
                goAbility(HttpRequestCallbackStringAbility.class);
                break;
            case ResourceTable.Id_btn_api_jsonobject:
                goAbility(HttpRequestCallbackJsonAbility.class);
                break;
            case ResourceTable.Id_btn_dm:
                Uri uri = Uri.parse("https://github.com/pengjianbo/FileDownloaderFinal");
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAction(IntentConstants.ACTION_APPLICATION_DETAILS_SETTINGS)
                        .withUri(uri)
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                new ToastDialog(MainAbility.this).setText("HM不支持跳转到系统浏览器").show();
                break;
            case ResourceTable.Id_btn_upload:
                checkPermission(2);
                break;
            case ResourceTable.Id_btn_download:
                checkPermission(1);
                break;
            case ResourceTable.Id_btn_other_funcation:
                goAbility(OtherFuncationAbility.class);
                break;
        }
    }

    private void goAbility(Class ability) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(ability.getName())
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }


    private void checkPermission(int flag) {
        if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
            // 应用未被授予权限
            if (canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                // 是否可以申请弹框授权(首次申请或者用户未选择禁止且不再提示)
                requestPermissionsFromUser(
                        new String[]{"ohos.permission.WRITE_USER_STORAGE"}, flag);
            } else {
                // 显示应用需要权限的理由，提示用户进入设置授权
                new ToastDialog(this).setText("您拒绝了存储权限，无法读取图片！").show();
            }
        } else {
            if(flag==1){
                goAbility(DownloadAbility.class);
            }else if(flag==2){
                goAbility(UploadAbility.class);
            }
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int i = 0; i < permissions.length; i++) {
            if (grantResults[i] == IBundleManager.PERMISSION_GRANTED) {
                if (requestCode == 1) {
                    goAbility(DownloadAbility.class);
                } else if (requestCode == 2) {
                    goAbility(UploadAbility.class);
                }
            } else {
                new ToastDialog(this).setText("您拒绝了存储权限，无法读取图片！").show();
            }
        }
    }

//    public static void initImageLoader(Context context) {
//        // This configuration tuning is custom. You can tune every option, you may tune some of them,
//        // or you can create default configuration by
//        //  ImageLoaderConfiguration.createDefault(this);
//        // method.
//        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
//        config.threadPriority(Thread.NORM_PRIORITY - 2);
//        config.denyCacheImageMultipleSizesInMemory();
//        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
//        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
//        config.tasksProcessingOrder(QueueProcessingType.LIFO);
//        config.writeDebugLogs(); // Remove for release app
//
//        // Initialize ImageLoader with configuration.
//        ImageLoader.getInstance().init(config.build());
//    }

}
