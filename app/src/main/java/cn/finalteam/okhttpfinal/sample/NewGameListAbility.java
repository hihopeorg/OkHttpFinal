package cn.finalteam.okhttpfinal.sample;

import cn.finalteam.okhttpfinal.HttpRequest;
import cn.finalteam.okhttpfinal.RequestParams;
import cn.finalteam.okhttpfinal.Utils;
import cn.finalteam.okhttpfinal.sample.adapter.NewGameListAdapter;
import cn.finalteam.okhttpfinal.sample.http.MyBaseHttpRequestCallback;
import cn.finalteam.okhttpfinal.sample.http.model.GameInfo;
import cn.finalteam.okhttpfinal.sample.http.model.NewGameResponse;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

public class NewGameListAbility extends BaseAbility {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "NewGameListAbility");

    NewGameListAdapter mProvider;
    private int mPage = 1;
    List<GameInfo> mGameList;
    ListContainer listContainer;
    private Text result;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_new_game_list);
        listContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        mGameList = new ArrayList<>();
        mProvider = new NewGameListAdapter(mGameList, NewGameListAbility.this);
        listContainer.setItemProvider(mProvider);
        requestData();
         result= (Text) findComponentById(ResourceTable.Id_tv_result);
    }

    private void requestData() {
        RequestParams params = new RequestParams(this);
//        params.addFormDataPart("page", 1);
//        params.addFormDataPart("limit", 12);
        HttpRequest.post("https://www.wanandroid.com/banner/json", params, new MyBaseHttpRequestCallback<NewGameResponse>() {
            @Override
            public void onLogicSuccess(NewGameResponse newGameResponse) {
                if (newGameResponse.getData() != null) {
                    List<GameInfo> data = newGameResponse.getData();
                    mGameList.addAll(data);
                    mProvider.notifyDataChanged();
                    StringBuffer buffer=new StringBuffer();
                    for (int i = 0; i < data.size(); i++) {
                        buffer.append(data.get(i).toString()+"\n\n");
                    }
                    result.setText(buffer.toString());
                } else {
                    new ToastDialog(getContext()).setText(newGameResponse.getMsg()).show();
                }
            }

            @Override
            public void onLogicFailure(NewGameResponse newGameResponse) {
                super.onLogicFailure(newGameResponse);
                String msg = newGameResponse.getMsg();
                if (msg.isEmpty()) {
                    msg = "网络异常";
                }
                new ToastDialog(getContext()).setText(msg).show();
            }

            @Override
            public void onFailure(int errorCode, String msg) {
                super.onFailure(errorCode, msg);
                new ToastDialog(getContext()).setText("网络异常").show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
            }
        });
    }
}
