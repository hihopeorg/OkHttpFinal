package cn.finalteam.okhttpfinal.sample.adapter;

import cn.finalteam.okhttpfinal.sample.ResourceTable;
import cn.finalteam.okhttpfinal.sample.http.MyHttpCycleContext;
import cn.finalteam.okhttpfinal.sample.http.model.GameInfo;
import com.bumptech.glide.Glide;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.List;

public class NewGameListAdapter extends BaseProvider<GameInfo> {

    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x00201, "PictureItemProvider");
    private Context mContext;

    public NewGameListAdapter(List<GameInfo> data, MyHttpCycleContext context) {
        super(data, context);
        mContext = context.getContext();

    }


    @Override
    protected int getLayoutResId() {
        return ResourceTable.Layout_adapter_new_game_list_item;
    }

    @Override
    protected int[] bindView() {
        return new int[]{ResourceTable.Id_img};
    }

    @Override
    protected void initData(ViewHolder holder, GameInfo entity, int position) {
        HiLog.info(label, "url---" + entity.getImagePath());
        Image image = holder.getViewById(ResourceTable.Id_img);
        Glide.with(mContext)
                .load(entity.getImagePath())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(image);
    }

}
