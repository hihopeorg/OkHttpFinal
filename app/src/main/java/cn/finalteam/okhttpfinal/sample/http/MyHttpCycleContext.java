package cn.finalteam.okhttpfinal.sample.http;

import cn.finalteam.okhttpfinal.HttpCycleContext;
import ohos.app.Context;

/**
 * Desction:
 * Author:pengjianbo
 * Date:15/9/26 下午6:05
 */
public interface MyHttpCycleContext extends HttpCycleContext{
     Context getContext();
}
