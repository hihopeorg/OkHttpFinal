package cn.finalteam.okhttpfinal.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class OtherFuncationAbility extends Ability {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_other_funcation);
    }

}
