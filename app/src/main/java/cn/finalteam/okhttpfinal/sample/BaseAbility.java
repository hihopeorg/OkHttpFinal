package cn.finalteam.okhttpfinal.sample;

import cn.finalteam.okhttpfinal.HttpTaskHandler;
import cn.finalteam.okhttpfinal.sample.http.MyHttpCycleContext;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class BaseAbility extends Ability implements MyHttpCycleContext {
    protected final String HTTP_TASK_KEY = "HttpTaskKey_" + hashCode();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public String getHttpTaskKey() {
        return HTTP_TASK_KEY;
    }

    @Override
    protected void onStop() {
        super.onStop();
        HttpTaskHandler.getInstance().removeTask(HTTP_TASK_KEY);
    }

}
