package cn.finalteam.okhttpfinal.sample;

import cn.finalteam.okhttpfinal.FileDownloadCallback;
import cn.finalteam.okhttpfinal.HttpRequest;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ProgressBar;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Environment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.PacMap;

import java.io.File;

public class DownloadAbility extends BaseAbility {
    private static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP,
            0x00201, "DownloadAbility");

    private   ProgressBar mPbDownload;
    Text tvProgress;
    MyHandler myHandler;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_download);
        initView();
    }

    public void initView() {
        Button download = (Button) findComponentById(ResourceTable.Id_bt_download);
        tvProgress = (Text) findComponentById(ResourceTable.Id_tv_progress);
        mPbDownload = (ProgressBar) findComponentById(ResourceTable.Id_pb_download);
        download.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                download();
                tvProgress.setText("正在下载--");
            }
        });
        myHandler = new MyHandler(EventRunner.getMainEventRunner());
    }

    public void download() {
        String url = "https://imtt.dd.qq.com/16891/apk/049319033C352F315594F5DD4ACC2911.apk";
        String path = this.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        String filePath = path + File.separator + "moji.apk";
        HttpRequest.download(url, new File(filePath), new FileDownloadCallback() {
            @Override
            public void onStart() {
                super.onStart();
                HiLog.info(label, "start--");
            }

            @Override
            public void onProgress(int progress, long networkSpeed) {
                super.onProgress(progress, networkSpeed);
                InnerEvent innerEvent = InnerEvent.get();
                innerEvent.eventId = 1;
                PacMap map = new PacMap();
                map.putIntValue("progress", progress);
                innerEvent.setPacMap(map);
                myHandler.sendEvent(innerEvent);
                HiLog.info(label, "progress--" + progress);
                //String speed = FileUtils.generateFileSize(networkSpeed);
            }

            @Override
            public void onFailure() {
                super.onFailure();
                InnerEvent innerEvent = InnerEvent.get();
                innerEvent.eventId = 3;
                myHandler.sendEvent(innerEvent);
            }

            @Override
            public void onDone() {
                super.onDone();
                InnerEvent innerEvent = InnerEvent.get();
                innerEvent.eventId = 2;
                myHandler.sendEvent(innerEvent);
                HiLog.info(label, "onDone--");
            }
        });
    }


    class MyHandler extends EventHandler {

        public MyHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            switch (event.eventId) {
                case 1:
                    int progress = event.getPacMap().getIntValue("progress");
                    mPbDownload.setProgressValue(progress);
                    tvProgress.setText("当前进度--" + progress + "%");
                    break;
                case 2:
                    new ToastDialog(DownloadAbility.this).setText("下载成功").show();
                    break;
                case 3:
                    new ToastDialog(DownloadAbility.this).setText("下载失败").show();
                    break;
            }
        }
    }
}
